/**
 * Created by axg on 4/25/2016.
 */

package com.hayhouse.hayhouseradio;

public class Config {

    public static final String HAYHOUSE_URL = "http://www.hayhouseradio.com/";

    public static final String LIVESTREAM_URL = "http://www.hayhouseradio.com/api/auth/mobile_auth_stream";

    public static final String CURRENT_EPISODE_URL = "http://www.hayhouseradio.com/api/episodes/current_episode.json";

    public static final String NEXT_EPISODE_URL = "http://www.hayhouseradio.com/api/episodes/next_episode.json";

    public interface ACTION {
        public static String MAIN_ACTION = "com.hayhouse.hayhouseradio.foregroundservice.action.main";
        public static String PREV_ACTION = "com.hayhosue.hayhouseradio.foregroundservice.action.prev";
        public static String PLAY_ACTION = "com.hayhouse.hayhouseradio.foregroundservice.action.play";
        public static String NEXT_ACTION = "com.hayhouse.hayhouseradio.foregroundservice.action.next";
        public static String STARTFOREGROUND_ACTION = "com.hayhouse.hayhouseradio.foregroundservice.action.startforeground";
        public static String STOPFOREGROUND_ACTION = "com.hayhouse.hayhouseradio.foregroundservice.action.stopforeground";
    }

    public interface NOTIFICATION_ID {
        public static int FOREGROUND_SERVICE = 101;
    }
}
