package com.hayhouse.hayhouseradio;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

//import com.hayhouse.hayhouseradio.Config;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private TextView mSelectedTrackTitle;
    private TextView mCurrentAuthorLabel;
    private ImageView mSelectedTrackImage;
    private ImageView mBigCurAuthor;
    private TextView mCurrentAuthorText;

    private MediaPlayer mMediaPlayer;
    private ImageView mPlayerControl;
    private String streamURL;
    private String CurAuthoorURL;
    private Bitmap CurAuthorPic;
    private TextView mCurrentShowTitleText;

    private Timer RefreshTimer;
    private TimerTask RefreshTimerTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        streamURL = "http://hhradioapp-lh.akamaihd.net/i/HHRadio2016_1_1@335868/master.m3u8";

        mSelectedTrackTitle = (TextView)findViewById(R.id.cur_episode_title);
        mCurrentAuthorLabel = (TextView)findViewById(R.id.current_author_label);
        mCurrentAuthorText = (TextView)findViewById(R.id.current_author_text);
        //mSelectedTrackImage = (ImageView)findViewById(R.id.selected_track_image);
        mPlayerControl = (ImageView)findViewById(R.id.player_control);
        mBigCurAuthor = (ImageView)findViewById(R.id.cur_author_big);

        mCurrentShowTitleText = (TextView) findViewById(R.id.current_showtitle_text);


        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                //togglePlayPause();
                mMediaPlayer.start();
            }
        });

        try {
            mMediaPlayer.setDataSource(streamURL);
            mMediaPlayer.prepareAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //mMediaPlayer.start();

        mPlayerControl.setImageResource(R.drawable.ic_pause);

        mPlayerControl = (ImageView)findViewById(R.id.player_control);

        mPlayerControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                togglePlayPause();
            }
        });

        getCurEpisodeInfo();

        Intent startIntent = new Intent(MainActivity.this, ForegroundService.class);
        startIntent.setAction(Config.ACTION.STARTFOREGROUND_ACTION);
        startService(startIntent);


    }

    private void togglePlayPause() {
        Intent startIntent = new Intent(MainActivity.this, ForegroundService.class);

        if (mMediaPlayer.isPlaying()) {
            mMediaPlayer.pause();
            mMediaPlayer.stop();
            mPlayerControl.setImageResource(R.drawable.ic_play);
            startIntent.setAction(Config.ACTION.STOPFOREGROUND_ACTION);
            stopService(startIntent);
        } else {
            //mMediaPlayer = new MediaPlayer();
            //mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            /*mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    //togglePlayPause();
                    mMediaPlayer.start();
                }
            });*/

            try {
                //mMediaPlayer.setDataSource(streamURL);
                mMediaPlayer.prepareAsync();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //mMediaPlayer.start();
            mPlayerControl.setImageResource(R.drawable.ic_pause);
            startIntent.setAction(Config.ACTION.STARTFOREGROUND_ACTION);
            startService(startIntent);
        }
    }

    /*
    @Override
    protected void onDestroy() {
        if (mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer = null;
        }
        super.onDestroy();
    }*/

    public void startTimer(){
        RefreshTimer = new Timer();
        RefreshTimerTask = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        getCurEpisodeInfo();
                    }
                });
            }
        };

        RefreshTimer.scheduleAtFixedRate(RefreshTimerTask, 60000, 60000);
    }

    public void getCurEpisodeInfo() {
        new bgGetCurEpisodeInfo().execute();
    }

    public class bgGetCurEpisodeInfo extends AsyncTask<String, String, String> {

        HttpURLConnection urlConnection;

        @Override
        protected String doInBackground(String... args) {

            StringBuilder result = new StringBuilder();


            try {
                URL url = new URL(Config.CURRENT_EPISODE_URL);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                BufferedReader reader = new BufferedReader(new InputStreamReader(in));

                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }

            }catch( Exception e) {
                e.printStackTrace();
            }
            finally {
                urlConnection.disconnect();
            }


            try {
                JSONObject json = new JSONObject(result.toString());
                CurAuthoorURL = "http://www.hayhouseradio.com/img/hosts/" + json.getString("author_id") + ".jpg";
            } catch (JSONException e) {
                Log.d(TAG, "FML: " + e);
            }

            try {
                URL newurl = new URL(CurAuthoorURL);
                try {
                    CurAuthorPic = BitmapFactory.decodeStream(newurl.openConnection().getInputStream());
                } catch (IOException e) {
                    Log.d(TAG, "UGH: " + e);
                }
            } catch (MalformedURLException e) {
                Log.d(TAG, "FML: " + e);
            }

            return result.toString();
        }

        @Override
        protected void onPostExecute(String result) {

            //Do something with the JSON string
            try {
                JSONObject json = new JSONObject(result);
                Log.i(TAG, "Episode: " + json.getString("episode_title"));
                mSelectedTrackTitle.setText(json.getString("show_title"));

                mCurrentAuthorText.setText(json.getString("author_name"));
                mCurrentShowTitleText.setText(json.getString("episode_title"));
                //CurAuthoorURL = "http://www.hayhouseradio.com/img/hosts/" + json.getString("author_id") + ".jpg";
                //new bgGetCurAuthorPic().execute();
                //mSelectedTrackImage.setImageBitmap(CurAuthorPic);
                mBigCurAuthor.setImageBitmap(CurAuthorPic);

            } catch (JSONException e) {
                Log.d(TAG, "FML: " + e);
            } finally {
                mCurrentAuthorLabel.setText("Current Episode:");
           }




        }

    }


}


/*
https://www.google.com/search?sourceid=chrome-psyapi2&ion=1&espv=2&ie=UTF-8&q=android%20mediaplayer%20audio%20stream%20foreground%20service&oq=android%20mediaplayer%20audio%20stream%20foreground%20service&aqs=chrome..69i57.7383j0j7

https://www.google.com/search?espv=2&biw=1670&bih=934&q=android+media+player+m3u8+audio&oq=android+media+player+m3u8+audio&gs_l=serp.3..33i21.3709.5242.0.5363.6.6.0.0.0.0.158.401.3j1.4.0....0...1c.1.64.serp..2.4.400.hIjhk4gfgII
http://www.hayhouseradio.com/api/auth/mobile_auth_stream
http://developer.android.com/guide/topics/media/mediaplayer.html
http://developer.android.com/reference/android/media/MediaPlayer.html#setDataSource(android.content.Context, android.net.Uri)
http://www.codota.com/android/methods/android.media.MediaPlayer/setDataSource
http://www.codota.com/android/scenarios/52fcbcdada0a1f9f8d94b852/android.media.MediaPlayer?tag=dragonfly
https://github.com/novoda/android-demos/blob/3052e6d253bd6a29c3e9b043ba87021242857ba5/simperAudioStreamer/src/novoda/audio/service/AudioStreamService.java
http://lyle.smu.edu/~coyle/cse7392mobile/handouts/s01.The%20AndroidManifest.pdf
 */