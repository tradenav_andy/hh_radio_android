package com.hayhouse.hayhouseradio;

/**
 * Created by axg on 4/25/2016.
 */
public class EpisodeResponse {
    String schedule_id;
    String show_id;
    String episode_id;
    String show_title;
    String show_slug;
    String episode_start;
    String episode_end;
    String original_episode_start;
    String original_episode_end;
    String original_episode_starttime;
    String original_episode_endtime;
    String original_db_episode_starttime;
    String original_db_episode_endtime;
    String current_server_datetime;
    String episode_title;
    String episode_slug;
    String description_short;
    String description_long;
    String phone1;
    String phone2;
    String display;
    String episode_create_date;
    String episode_revise_date;
    String author_id;
    String author_name;
    String author_slug;
    String show_day;
    String original_show_start;
    String original_show_end;
    String converted_show_start;
    String converted_show_end;
    String show_day_name;
    String is_original_episiode;
    String is_currently_playing;
    String has_aired;
}
